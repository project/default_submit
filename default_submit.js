
/**
 * @file
 * JS Helper that creates an invisible button using the default submit button
 * clone in order for browsers to do the real default actions when user press
 * the enter key. 
 */

(function ($) {

/*
 * Using the "normal" method, live method that introspect the DOM in order
 * to find the default submit handler.
 * This may be performance eater, so we are going to avoid it if the second
 * method works.
 * 
Drupal.behaviors.FormDefaultSubmit = function(context) {
  $("form input, form select", context).live("keypress", function (e) {
    var defaultSubmitAction = $(this).parents('form').find('button[type=submit].default-submit-action, input[type=submit].default-submit-action');
    if (defaultSubmitAction.length > 0) {
      if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        defaultSubmitAction.click();
        return false;
      }
    }
    return true;
  });
};
 */

/*
 * Using the button clone method, this should work better because we don't have
 * to handle ourself the keycode signal, which nor nomalized by the way. And it
 * also avoid to introspect the DOM at each keypress, which should be a lot
 * faster to run on many browsers.
 * The cons of this method is we play with CSS and we could break some layout
 * and stylish stuff, needs some testing.
 */

Drupal.behaviors.FormDefaultSubmit = function(context) {
  $("form:not(.default-submit-action-processed)", context).each(function() {
    var form = $(this);
    var submit = $("input.default-submit-action:first", this);
    if (submit) {
      form.prepend(submit.clone().css({
        position: "absolute",
        // Really huge values to ensure this button goes far far away.
        left: "-10000px",
        top: "-10000px",
        // Ensure it won't get useless space.
        height: "0px",
        width: "0px"
      }));
    }
    form.addClass("default-submit-action-processed");
  });
};

})(jQuery);
